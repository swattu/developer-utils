#!/usr/bin/env bash
FOLDER=`dirname "$0"`
cd ${FOLDER}/..
mvn clean install
cd ${FOLDER}/../aws-dev
java -cp src/main/resources:target/aws.jar:target/lib/* org.swat.aws.SecurityUtils "$@"
