package org.swat.mongo;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.*;

@Service
public class SchemaGenerator {
    @Autowired
    private MongoTemplate mongoTemplate;
    private final Map<Class, String> classMap = new HashMap<>();
    private final Map<String, Set<String>> classFields = new HashMap<>();
    private final Set<String> pendingCollections = new HashSet<>();
    private final Map<String, List<String>> classLines = new HashMap<>();

    {
        classMap.put(ArrayList.class, "List");
        classMap.put(ObjectId.class, "String");
        classMap.put(LinkedHashMap.class, "Map");
    }

    @PostConstruct
    public void generateSchema() throws Exception {
        Set<String> collectionNames = mongoTemplate.getCollectionNames();
        for (int iteration = 0; iteration <= 5; iteration++) {
            for (String collectionName : collectionNames) {
                generateSchema(collectionName, iteration);
            }
            collectionNames = new HashSet<>(pendingCollections);
            pendingCollections.clear();
        }
        for (Map.Entry<String, List<String>> entry : classLines.entrySet()) {
            writeTo(entry.getKey(), entry.getValue());
        }
        System.exit(1);
    }

    private void generateSchema(String collectionName, int iteration) throws Exception {
        String fileName = StringUtils.capitalize(collectionName);
        List<String> lines = classLines.computeIfAbsent(fileName, v -> new ArrayList<>());
        Query query = new Query();
        query.limit(1);
        List<Map> records = mongoTemplate.find(query, Map.class, collectionName);
        for (Map<String, Object> record : records) {
            createPojo(fileName, record, iteration);
        }
    }

    private void writeTo(String fileName, List<String> fields) throws IOException {
        fileName = sanitize(fileName);
        List<String> lines = new ArrayList<>();
        lines.add("package org.swat.generated;");
        lines.add("import com.hilyar.entity.bean.BaseEntity;");
        lines.add("import java.util.Date;");
        lines.add("import java.util.List;");
        lines.add("import java.util.Map;");
        lines.add("import java.time.LocalDate;");
        lines.add("import java.time.LocalDateTime;");
        lines.add("import lombok.Data;");
        lines.add("import org.springframework.data.mongodb.core.mapping.Field;");
        lines.add("import com.fasterxml.jackson.annotation.JsonIgnoreProperties;");
        lines.add("@Data");
        lines.add("@JsonIgnoreProperties(ignoreUnknown = true)");
        lines.add("public class " + fileName + " extends BaseEntity {");
        lines.addAll(fields);
        lines.add("}");
        FileUtils.writeLines(new File("mongo-re/src/main/java/org/swat/generated", fileName + ".java"), lines);
    }

    private String nameFromObject(String fieldName, Object value, int iteration) throws Exception {
        if (value instanceof List) {
            return nameFromList(fieldName, (List) value, iteration);
        }
        if (value instanceof Map) {
            return nameFromMap(fieldName, (Map) value, iteration);
        }
        return deriveClassName(value);
    }

    private String deriveClassName(Object value) {
        if (value == null)
            return "Object";
        if (value instanceof Date) {
            long epoch = ((Date) value).getTime();
            if (epoch % 86400000 == 0) {
                return "LocalDate";
            }
            return "LocalDateTime";
        }
        Class clazz = value.getClass();
        String className = classMap.get(clazz);
        if (className == null) {
            className = clazz.getSimpleName();
        }
        return className;
    }

    private String nameFromMap(String fieldName, Map<String, Object> map, int iteration) throws Exception {
        for (Map.Entry<String, Set<String>> classField : classFields.entrySet()) {
            if (classField.getValue().containsAll(map.keySet())) {
                return classField.getKey();
            }
        }

        if (iteration == 5) {
            Set<String> types = new HashSet<>();
            for (Map.Entry<String, Object> classField : map.entrySet()) {
                types.add(nameFromObject(fieldName, classField.getValue(), iteration));
            }
            if (types.size() == 1) {
//                return "Map<String, " + types.iterator().next() + ">";
            }
            return createPojo(fieldName, map, iteration);
        }
        return null;
    }

    private String sanitize(String value) {
        if (value == null)
            return null;
        return value.replaceAll("-", "");
    }

    private String createPojo(String fieldName, Map<String, Object> record, int iteration) throws Exception {
        fieldName = sanitize(fieldName);
        String fileName = StringUtils.capitalize(fieldName);
        List<String> lines = classLines.computeIfAbsent(fileName, v -> new ArrayList<>());
        Set<String> fields = classFields.computeIfAbsent(fileName, v -> new HashSet<>());
        record.keySet().forEach(v -> fields.add(sanitize(v)));
        if (fields.contains("_id")) {
            fields.add("id");
        }
        for (Map.Entry<String, Object> entry : record.entrySet()) {
            if (entry.getValue() == null)
                continue;
            String key = entry.getKey();
            if ("_id".equals(key)) {
                key = "id";
            }
            String sanitizedKey = sanitize(key);
            String className = nameFromObject(sanitizedKey, entry.getValue(), iteration);
            if (className == null) {
                pendingCollections.add(fieldName);
                return null;
            }
            String fieldLine = null;
            if ("default".equalsIgnoreCase(sanitizedKey)) {
                fieldLine = "@Field(\"default\")";
                sanitizedKey = "_default";
            } else if (!sanitizedKey.equals(key)) {
                fieldLine = "@Field(\"" + key + "\")";
            }
            String field = "private " + className + " " + sanitizedKey + ";";
            if (!lines.contains(field)) {
                if (fieldLine != null)
                    lines.add(fieldLine);
                lines.add(field);
            }
        }
        return fileName;
    }

    private String nameFromList(String fieldName, List list, int iteration) throws Exception {
        if (list.isEmpty()) {
            return "List";
        }
        Object value = list.get(0);
        String className = nameFromObject(fieldName, value, iteration);
        if (className == null) {
            return null;
        }
        return "List<" + className + ">";
    }
}
