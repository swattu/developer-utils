/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.aws;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.swat.core.utils.CoreRtException;
import org.swat.excel.utils.NamedSectionSheet;
import org.swat.http.utils.HttpClientHelper;
import org.swat.http.utils.HttpClientUtils;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.ec2.model.*;

import java.io.*;
import java.util.*;

import static software.amazon.awssdk.auth.credentials.AwsBasicCredentials.create;

/**
 * The type Security utils.
 */
@Slf4j
public class SecurityUtils {
    private String ipAddress;
    private final Properties properties = new Properties();

    {
        File file = new File("history.properties");
        if (file.exists()) {
            try {
                properties.load(new FileInputStream(file));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        InputStream inputStream = Ec2Client.class.getClassLoader().getResourceAsStream("SecurityConfig.xlsx");
        if (inputStream == null) {
            System.out.println("File SecurityConfig.xlsx not found.");
            System.out.println("1. Copy SecurityConfigTemplate.xlsx as SecurityConfig.xlsx in the same directory");
            System.out.println("2. Configure records in SecurityConfig.xlsx, as per the headers.");
            System.out.println("3. Execute this program again.\n");
            System.out.println("** All fields are mandatory.");
            System.out.println("** For all GroupId's, use ALL.");
            System.out.println("** ");
            throw new CoreRtException(", configure the properties and execute this program again.");

        }
        Workbook workbook = new XSSFWorkbook(inputStream);
        NamedSectionSheet sheet = new NamedSectionSheet(workbook.getSheet("Inbound"));
        SecurityUtils securityUtils = new SecurityUtils();
        NamedSectionSheet.Section section = sheet.getSection(0);
        Iterator<NamedSectionSheet.Section.NamedRow> iterator = section.iterator();
        while (iterator.hasNext()) {
            securityUtils.updateSecurities(iterator.next());
        }
        securityUtils.close();
    }

    private void close() throws Exception {
        properties.setProperty("last.ip.address", ipAddress);
        properties.save(new FileOutputStream("history.properties"), "UTF-8");
    }

    private String getValue(NamedSectionSheet.Section.NamedRow row, String key) {
        String value = row.asString(key);
        if (StringUtils.isBlank(value)) {
            throw new CoreRtException(key + " is mandatory");
        }
        return value.trim();
    }

    public void updateSecurities(NamedSectionSheet.Section.NamedRow row) {
        final String key = getValue(row, "Key");
        if (StringUtils.isBlank(key)) {
            throw new CoreRtException("Key is mandatory.");
        }
        final String secret = getValue(row, "Secret");
        if (StringUtils.isBlank(secret)) {
            throw new CoreRtException("Secret is mandatory.");
        }
        String regionStr = getValue(row, "Region");
        if (StringUtils.isBlank(regionStr)) {
            throw new CoreRtException("Region is mandatory. Valid values: " + Region.regions());
        }
        final Region region = Region.of(StringUtils.lowerCase(regionStr));

        final Ec2Client ec2Client = Ec2Client.builder().credentialsProvider(() -> create(key, secret)).region(region).build();
        String groupId = getValue(row, "GroupId");
        if (StringUtils.isBlank(groupId)) {
            throw new CoreRtException("GroupId is mandatory. ALL for all groupIds.");
        }
        if (StringUtils.equalsIgnoreCase(groupId, "all")) {
            groupId = null;
        }
        final String description = getValue(row, "Description");
        if (StringUtils.isBlank(description)) {
            throw new CoreRtException("Description is mandatory. It is used to remove older ip permissions.");
        }
        final int port = row.asInt("Port");
        if (port == 0) {
            throw new CoreRtException("Port is mandatory.");
        }

        DescribeSecurityGroupsRequest.Builder describeBuilder = DescribeSecurityGroupsRequest.builder();
        if (StringUtils.isNotBlank(groupId)) {
            describeBuilder.groupIds(groupId);
        }
        DescribeSecurityGroupsRequest securityGroupsRequest = describeBuilder.build();
        DescribeSecurityGroupsResponse response = ec2Client.describeSecurityGroups(securityGroupsRequest);
        final String ipAddress = getIpAddress() + "/32";
        for (SecurityGroup securityGroup : response.securityGroups()) {
            groupId = securityGroup.groupId();
            final String groupName = securityGroup.groupName();
            Set<String> revokeAddresses = new HashSet<>();
            boolean same = false;
            List<IpPermission> ipPermissions = securityGroup.ipPermissions();
            for (IpPermission ipPermission : ipPermissions) {
                if (ipPermission == null || ipPermission.fromPort() == null || ipPermission.toPort() == null) {
                    continue;
                }
                if (port != ipPermission.fromPort() && port != ipPermission.toPort()) {
                    continue;
                }
                for (IpRange ipRange : ipPermission.ipRanges()) {
                    if (StringUtils.equals(ipRange.cidrIp(), ipAddress)) {
                        same = true;
                    } else if (StringUtils.equals(ipRange.description(), description)) {
                        revokeAddresses.add(ipRange.cidrIp());
                    }
                }
            }
            for (String revokeAddress : revokeAddresses) {
                if (StringUtils.isNotBlank(revokeAddress)) {
                    String dateKey = "date." + revokeAddress;
                    String whenAdded = properties.getProperty(dateKey);
//if (NumberUtils.toLong(whenAdded) < System.currentTimeMillis() - 86400L * 30000)
                    {
                        RevokeSecurityGroupIngressRequest.Builder revokeBuilder = RevokeSecurityGroupIngressRequest.builder();
                        revokeBuilder.groupId(groupId);
                        revokeBuilder.ipProtocol("tcp");
                        revokeBuilder.cidrIp(revokeAddress);
                        revokeBuilder.fromPort(port);
                        revokeBuilder.toPort(port);
                        ec2Client.revokeSecurityGroupIngress(revokeBuilder.build());
                        log.info("{}\t{} Revoked access for {}", groupName, groupId, revokeAddress);
                        properties.remove(dateKey);
                        properties.remove("first." + revokeAddress);
                    }
                }
            }
            String dateKey = "date." + ipAddress;
            if (!same) {
                IpRange.Builder ipRangeBuilder = IpRange.builder();
                ipRangeBuilder.cidrIp(ipAddress);
                ipRangeBuilder.description(description);

                IpPermission.Builder ipPermissionBuilder = IpPermission.builder();
                ipPermissionBuilder.ipProtocol("tcp");
                ipPermissionBuilder.fromPort(port);
                ipPermissionBuilder.toPort(port);
                ipPermissionBuilder.ipRanges(ipRangeBuilder.build());

                AuthorizeSecurityGroupIngressRequest.Builder authorizeBuilder = AuthorizeSecurityGroupIngressRequest.builder();
                authorizeBuilder.groupId(groupId);
                authorizeBuilder.ipPermissions(ipPermissionBuilder.build());
                ec2Client.authorizeSecurityGroupIngress(authorizeBuilder.build());
                log.info("{}\t{} Authorized access for {}", groupName, groupId, ipAddress);
            } else {
                log.info("{}\t{} Access is already present for {}", groupName, groupId, ipAddress);
            }
            properties.setProperty(dateKey, System.currentTimeMillis() + "");
            if (!properties.containsKey("first." + ipAddress)) {
                properties.setProperty("first." + ipAddress, new Date() + "");
            }
        }
    }

    private String getIpAddress() {
        if (ipAddress != null) {
            return ipAddress;
        }
        HttpClient client = HttpClientUtils.getClient(null, null);
        HttpGet method = new HttpGet("http://ipecho.net/plain");
        HttpClientHelper helper = new HttpClientHelper(client);
        String ipAddress = helper.executeJson(method);
        if (StringUtils.isBlank(ipAddress)) {
            throw new CoreRtException("Could not find Ip Address");
        }
        this.ipAddress = ipAddress;
        return ipAddress;
    }
}
