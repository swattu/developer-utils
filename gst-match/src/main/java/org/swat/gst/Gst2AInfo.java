package org.swat.gst;

import lombok.Data;

import java.util.Date;
import java.util.Objects;

@Data
public class Gst2AInfo implements Comparable {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Gst2AInfo info = (Gst2AInfo) o;
        return Double.compare(info.taxable, taxable) == 0 &&
                Objects.equals(party, info.party) &&
                Objects.equals(invoiceNumber, info.invoiceNumber) &&
                Objects.equals(invoiceDate, info.invoiceDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(party, invoiceNumber, invoiceDate, taxable);
    }

    private String gstNo;
    private String party;
    private String txnParty;
    private String invoiceNumber;
    private Date invoiceDate;
    private double value;
    private double taxable;
    private double tallyTaxable;
    private double igst;
    private double cgst;
    private double sgst;
    private String comments;
    private String system;
    private String fileName;

    @Override
    public int compareTo(Object obj) {
        if (this == obj)
            return 0;
        Gst2AInfo that = (Gst2AInfo) obj;
        int status = compare(that.taxable, this.taxable);
        if (status != 0) {
            return status;
        }
        status = compare(this.invoiceDate, that.invoiceDate);
        if (status != 0) {
            return status;
        }
        status = compare(this.party, that.party);
        if (status != 0) {
            return status;
        }
        return compare(this.invoiceNumber, that.invoiceNumber);
    }

    private <T extends Comparable> int compare(T first, T second) {
        if (first == second)
            return 0;
        if (first == null)
            return -1;
        if (second == null)
            return 1;
        return first.compareTo(second);
    }
}
