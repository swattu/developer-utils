package org.swat.gst;

import lombok.Data;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

@Data
public class PartyInfo {
    private String gst;
    private String gstName;
    private String txnName;
    private AtomicInteger count = new AtomicInteger();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PartyInfo partyInfo = (PartyInfo) o;
        return Objects.equals(gst, partyInfo.gst) &&
                Objects.equals(gstName, partyInfo.gstName) &&
                Objects.equals(txnName, partyInfo.txnName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gst, gstName, txnName);
    }
}
