package org.swat.gst;

import lombok.Data;

import java.util.Date;

@Data
public class TransactionInfo implements Comparable {
    private int index;
    private String party;
    private Date date;
    private double taxable;
    private String comments;
    private String system;

    @Override
    public int compareTo(Object obj) {
        if (this == obj)
            return 0;
        TransactionInfo that = (TransactionInfo) obj;
        int status = compare(that.taxable, this.taxable);
        if (status != 0) {
            return status;
        }
        status = compare(this.date, that.date);
        if (status != 0) {
            return status;
        }
        status = compare(this.party, that.party);
        if (status != 0) {
            return status;
        }
        return compare(this.index, that.index);
    }

    private <T extends Comparable> int compare(T first, T second) {
        if (first == second)
            return 0;
        if (first == null)
            return -1;
        if (second == null)
            return 1;
        return first.compareTo(second);
    }
}
