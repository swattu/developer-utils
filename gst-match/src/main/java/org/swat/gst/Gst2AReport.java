package org.swat.gst;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.swat.csv.utils.DateFormatter;
import org.swat.csv.utils.XlsWriter;
import org.swat.date.utils.DateTimeUtil;
import org.swat.excel.utils.ExcelUtils;
import org.swat.excel.utils.NamedSectionSheet;
import org.swat.pdf.utils.PdfUtil;
import org.swat.regex.utils.MatchedRow;
import org.swat.regex.utils.RegexBuilder;
import org.swat.regex.utils.RegexInfo;
import org.swat.regex.utils.RegexParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.*;

import static org.swat.regex.utils.RegexBuilder.*;

public class Gst2AReport {
    private static final NumberFormat NF = NumberFormat.getInstance();
    private final String prefix;
    private Set<Gst2AInfo> gstInfos = new TreeSet<>();
    private Set<Gst2AInfo> existingInfos = new TreeSet<>();
    private Set<TransactionInfo> existingTxns = new TreeSet<>();
    private Map<String, Map<PartyInfo, PartyInfo>> partyInfoMap = new HashMap<>();
    private SimpleDateFormat DD_MM_YYYY = new SimpleDateFormat("dd-MM-yyyy");

    static {
        NF.setMinimumIntegerDigits(2);
    }

    public String getHomeFolder() {
        return System.getProperty("user.home");
    }

    public String getBaseFolder() {
        return getHomeFolder() + "/GST";
    }

    public String getBackupFolder() {
        return getHomeFolder() + "/GST-Backup";
    }

    public Gst2AReport() {
        ZonedDateTime time = ZonedDateTime.now();
        DateTimeUtil util = new DateTimeUtil("Asia/Kolkata");
        this.prefix = util.format(time, "yyyy-MM-dd HH:mm:ss") + "-";
    }

    public static void main(String[] args) throws Exception {
        System.out.println(System.getProperties());
        Gst2AReport report = new Gst2AReport();
        report.process();
    }

    public void process() throws Exception {
        Gst2AReport report = new Gst2AReport();

        report.readGstFiles(new File(getBaseFolder()));
        report.processPdf(new File(getBaseFolder(), "Purchase-Condensed.pdf"));
        report.readExcel();
        report.match();
        report.generateExcel();
    }

    private void match() {
        for (Gst2AInfo first : gstInfos) {
            for (Gst2AInfo second : existingInfos) {
                if (first.equals(second)) {
                    first.setComments(second.getComments());
                }
            }
        }
        existingInfos.clear();
        existingInfos.addAll(gstInfos);
        Set<Gst2AInfo> distinctInfos = distinctInfos(existingInfos);
        Set<Gst2AInfo> distinctTxns = distinctInfos(existingInfos);
        Iterator<Gst2AInfo> gstIterator = existingInfos.iterator();
        Set<TransactionInfo> txns = new TreeSet<>(existingTxns);
        while (gstIterator.hasNext()) {
            Gst2AInfo info = gstIterator.next();
            Iterator<TransactionInfo> txnIterator = txns.iterator();
            while ((txnIterator.hasNext())) {
                TransactionInfo txn = txnIterator.next();
                double diff = info.getTaxable() - txn.getTaxable();
                if (diff > 2) {
                    break;
                } else if (diff < -2) {
                    txnIterator.remove();
                } else {
                    Map<PartyInfo, PartyInfo> map = partyInfoMap.computeIfAbsent(info.getGstNo(), v -> new HashMap<>());
                    PartyInfo partyInfo = new PartyInfo();
                    partyInfo.setGst(info.getGstNo());
                    partyInfo.setGstName(info.getParty());
                    partyInfo.setTxnName(txn.getParty());

                    partyInfo = map.computeIfAbsent(partyInfo, v -> new PartyInfo());
                    partyInfo.setGst(info.getGstNo());
                    partyInfo.setGstName(info.getParty());
                    partyInfo.setTxnName(txn.getParty());
                    partyInfo.getCount().incrementAndGet();

                    if (info.getSystem() == null) {
                        txn.setSystem("Matched");
                        info.setSystem("Matched");
                        info.setTallyTaxable(txn.getTaxable());
                        txnIterator.remove();
                    }
                }
            }
        }
        while (true) {
            boolean removed = false;
            for (Gst2AInfo info : existingInfos) {
                Map<PartyInfo, PartyInfo> map = partyInfoMap.get(info.getGstNo());
                if (map != null && map.size() == 1) {
                    String txnName = new ArrayList<>(map.values()).get(0).getTxnName();
                    info.setTxnParty(txnName);
                    removed = removed | removeFromPartyMap(txnName);
                }
            }
            if (!removed) {
                break;
            }
        }

        for (Map.Entry<String, Map<PartyInfo, PartyInfo>> entry : partyInfoMap.entrySet()) {
            if (entry.getValue().size() > 1) {
                System.out.println(entry.getKey() + " ==> " + entry.getValue().values());
            }
        }
    }

    private Set<Gst2AInfo> distinctInfos(Set<Gst2AInfo> infos) {
        Set<Gst2AInfo> distinctInfos = new TreeSet<>();
        Gst2AInfo previous = null;
        for (Gst2AInfo info : infos) {
            if (previous == null) {
                previous = info;
            } else if (previous.getTaxable() - info.getTaxable() > 5) {
                distinctInfos.add(previous);
                previous = info;
            } else {
                previous = null;
            }
        }
        return distinctInfos;
    }

    private boolean removeFromPartyMap(String txnName) {
        boolean removed = false;
        for (Map<PartyInfo, PartyInfo> map : partyInfoMap.values()) {
            if (map.size() > 1) {
                for (PartyInfo partyInfo : new HashSet<>(map.keySet())) {
                    if (partyInfo.getTxnName().equals(txnName)) {
                        map.remove(partyInfo);
                        removed = true;
                    }
                }
            }
        }
        return removed;
    }

    private void processPdf(File file) throws Exception {
        FileUtils.copyFile(file, new File(getBackupFolder(), prefix + "In-Purchase Condensed.pdf"));
        List<String> lines = PdfUtil.parseLines(file);
        List<RegexInfo> infos = new ArrayList<>();

        RegexBuilder builder = RegexBuilder.builder();
        builder.append(chars("0-9-").min(0).name("date"));
        builder.append(chars(" ").min(0));
        builder.append(text("Cr"));
        builder.append(chars(" "));
        builder.append(anyChar().name("party"));
        builder.append(chars(" "));
        builder.append(amount().decimals(2).name("amount"));

        infos.add(builder.starts());

        RegexParser parser = new RegexParser(lines);
        parser.setRegexInfos(infos);
        List<MatchedRow> rows = parser.parse(true);
        Date date = null;
        int index = 0;
        for (MatchedRow row : rows) {
            index++;
            String dateStr = row.get("date");
            if (StringUtils.isNotBlank(dateStr)) {
                date = DD_MM_YYYY.parse(dateStr);
            }
            TransactionInfo info = new TransactionInfo();

            info.setIndex(index);
            info.setDate(date);
            info.setParty(row.get("party"));
            info.setTaxable(row.asDouble("amount"));
            existingTxns.add(info);
        }
    }

    private void readExcel() throws Exception {
        File file = new File(getBaseFolder(), "GST-2B Match Information.xlsx");
        if (!file.exists()) {
            return;
        }
        Workbook workbook = new XSSFWorkbook(file);
        Sheet sheet = workbook.getSheet("B2B");
        if (sheet == null) {
            return;
        }
        NamedSectionSheet sectionSheet = new NamedSectionSheet(sheet);
        NamedSectionSheet.Section section = sectionSheet.getSection(0);
        Iterator<NamedSectionSheet.Section.NamedRow> iterator = section.iterator();
        while (iterator.hasNext()) {
            NamedSectionSheet.Section.NamedRow row = iterator.next();
            Gst2AInfo info = new Gst2AInfo();
            info.setGstNo(row.asString("GST"));
            info.setParty(row.asString("Party"));
            info.setInvoiceNumber(row.asString("Invoice Number"));
            String dateStr = row.asString("Invoice Date");
            if (dateStr != null) {
                info.setInvoiceDate(DD_MM_YYYY.parse(dateStr));
            }
            info.setValue(row.asDouble("Total Value"));
            info.setTaxable(row.asDouble("Taxable Value"));
            info.setIgst(row.asDouble("IGST"));
            info.setCgst(row.asDouble("CGST"));
            info.setSgst(row.asDouble("SGST"));
            info.setComments(row.asString("Comments"));
            existingInfos.add(info);
        }
    }

    private void generateExcel() throws Exception {
        Workbook workbook = new XSSFWorkbook();
        XlsWriter<TransactionInfo> writer = new XlsWriter<>(TransactionInfo.class, workbook.createSheet("TXN"));
        writer.setDefaultFormatter(new DateFormatter(TimeZone.getDefault().getID(), "yyyy-MM-dd"));
        writer.write(existingTxns);
        ExcelUtils.autoSizeColumns(workbook);
        File output = new File(getBaseFolder(), "GST-Generated.xlsx");
        if (output.exists()) {
            output.renameTo(new File(getBackupFolder(), prefix + "In-GST-Generated.xlsx"));
        }
        workbook.write(new FileOutputStream(output));
        output = new File(getBackupFolder(), prefix + "Out-GST-Generated.xlsx");
        workbook.write(new FileOutputStream(output));

        existingInfos.addAll(gstInfos);
        workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("B2B");
        int rowNo = -1;
        Row row = sheet.createRow(++rowNo);
        int cellNo = -1;

        row.createCell(++cellNo).setCellValue("GST");
        row.createCell(++cellNo).setCellValue("Party");
        row.createCell(++cellNo).setCellValue("Txn Party");
        row.createCell(++cellNo).setCellValue("Invoice Number");
        row.createCell(++cellNo).setCellValue("Invoice Date");
        row.createCell(++cellNo).setCellValue("Total Value");
        row.createCell(++cellNo).setCellValue("Taxable Value");
        row.createCell(++cellNo).setCellValue("Taxable Tally");
        row.createCell(++cellNo).setCellValue("IGST");
        row.createCell(++cellNo).setCellValue("CGST");
        row.createCell(++cellNo).setCellValue("SGST");
        row.createCell(++cellNo).setCellValue("System");
        row.createCell(++cellNo).setCellValue("Comments");
        row.createCell(++cellNo).setCellValue("File Name");


        for (Gst2AInfo info : existingInfos) {
            row = sheet.createRow(++rowNo);
            cellNo = -1;
            row.createCell(++cellNo).setCellValue(info.getGstNo());
            row.createCell(++cellNo).setCellValue(info.getParty());
            row.createCell(++cellNo).setCellValue(info.getTxnParty());
            row.createCell(++cellNo).setCellValue(info.getInvoiceNumber());
            if (info.getInvoiceDate() != null)
                row.createCell(++cellNo).setCellValue(DD_MM_YYYY.format(info.getInvoiceDate()));
            else
                row.createCell(++cellNo).setCellValue("");
            row.createCell(++cellNo).setCellValue(info.getValue());
            row.createCell(++cellNo).setCellValue(info.getTaxable());
            row.createCell(++cellNo).setCellValue(info.getTallyTaxable());
            row.createCell(++cellNo).setCellValue(info.getIgst());
            row.createCell(++cellNo).setCellValue(info.getCgst());
            row.createCell(++cellNo).setCellValue(info.getSgst());
            row.createCell(++cellNo).setCellValue(info.getSystem());
            row.createCell(++cellNo).setCellValue(info.getComments());
            row.createCell(++cellNo).setCellValue(info.getFileName());
        }
        ExcelUtils.autoSizeColumns(workbook);
        output = new File(getBaseFolder(), "GST-2B Match Information.xlsx");
        if (output.exists()) {
            output.renameTo(new File(getBackupFolder(), prefix + "In-GST-2B Match Information.xlsx"));
        }
        workbook.write(new FileOutputStream(output));
        output = new File(getBackupFolder(), prefix + "Out-GST-2B Match Information.xlsx");
        workbook.write(new FileOutputStream(output));
    }

    private void readGstFiles(File file) throws Exception {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File tmp : files) {
                    readGstFiles(tmp);
                }
            }
            return;
        }
        if (!file.getName().toLowerCase().endsWith(".xlsx")) {
            return;
        }
        Workbook workbook = new XSSFWorkbook(new FileInputStream(file));
        Sheet sheet = workbook.getSheet("B2B");
        if (sheet == null)
            return;
        String fileName = file.getName();
        fileName = fileName.substring(fileName.lastIndexOf('/') + 1);
        readGstFiles(sheet, fileName);
    }

    private void readGstFiles(Sheet sheet, String fileName) throws Exception {
        Iterator<Row> rowIterator = sheet.rowIterator();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (row == null) {
                continue;
            }
            try {
                readGstFiles(row, fileName);
            } catch (Exception ignore) {
            }
        }
    }

    private void readGstFiles(Row row, String fileName) throws Exception {
        Iterator<Cell> cellIterator = row.cellIterator();
        Gst2AInfo info = new Gst2AInfo();
        while (cellIterator.hasNext()) {
            Cell cell = cellIterator.next();
            int index = cell.getColumnIndex();
            switch (index) {
                case 0:
                    info.setGstNo(cell.getStringCellValue());
                    break;
                case 1:
                    info.setParty(cell.getStringCellValue());
                    break;
                case 2:
                    info.setInvoiceNumber(cell.getStringCellValue());
                    break;
                case 4:
                    if (cell.getCellTypeEnum() == CellType.NUMERIC)
                        info.setInvoiceDate(cell.getDateCellValue());
                    else if (cell.getCellTypeEnum() == CellType.STRING) {
                        String dateStr = cell.getStringCellValue();
                        Date date = DD_MM_YYYY.parse(dateStr);
                        info.setInvoiceDate(date);
                    }
                    break;
                case 5:
                    if (cell.getCellTypeEnum() == CellType.NUMERIC)
                        info.setValue(cell.getNumericCellValue());
                    break;
                case 9:
                    if (cell.getCellTypeEnum() == CellType.NUMERIC)
                        info.setTaxable(cell.getNumericCellValue());
                    break;
                case 10:
                    if (cell.getCellTypeEnum() == CellType.NUMERIC)
                        info.setIgst(cell.getNumericCellValue());
                    break;
                case 11:
                    if (cell.getCellTypeEnum() == CellType.NUMERIC)
                        info.setCgst(cell.getNumericCellValue());
                    break;
                case 12:
                    if (cell.getCellTypeEnum() == CellType.NUMERIC)
                        info.setSgst(cell.getNumericCellValue());
                    break;
            }
        }
        if (info.getInvoiceNumber() != null && info.getInvoiceNumber().endsWith("-Total")) {
            info.setFileName(fileName);
            gstInfos.add(info);
        }
    }
}
